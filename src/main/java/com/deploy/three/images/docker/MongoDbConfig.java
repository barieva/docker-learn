package com.deploy.three.images.docker;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackageClasses = HelloEntity.class)
@Configuration
public class MongoDbConfig {
}
