package com.deploy.three.images.docker;

import javax.persistence.Id;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "user")
public class HelloEntity {
    @Id
    private String id;
    private String hello;

    public HelloEntity() { }
    public HelloEntity(String hello) {
        this.hello = hello;
    }

}
