package com.deploy.three.images.docker;


import org.springframework.data.mongodb.repository.MongoRepository;

public interface HelloRepository extends MongoRepository<HelloEntity,String> {
}
