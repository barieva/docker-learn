package com.deploy.three.images.docker;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    private HelloRepository helloRepository;

    @GetMapping("/hello")
    public String getHelloCountMessage() {
        long count = helloRepository.count();
        return "Total hello count is " + count;
    }

    @GetMapping("/sayhello")
    public String sayHello(@RequestParam("hello") String hello) {
        HelloEntity helloEntity = new HelloEntity(hello);
        helloRepository.save(helloEntity);
        return "You said hello";
    }
}
